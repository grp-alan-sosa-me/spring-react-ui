//EC2 docker 
//container endpoint const API_URL = "http://3.142.35.186:8080";
//local dev 
//const API_URL = "http://localhost:8080";
//Kubernetes service endpoint
const API_URL = "http://34.71.79.47:8080";
export const LOGIN_ENDPOINT = API_URL + "/users/login";
export const CREATE_USER_ENDPOINT = API_URL + "/users";
export const PUBLIC_POSTS_ENDPOINT = API_URL + "/posts/last";
export const POST_DETAILS_ENDPOINT = API_URL + "/posts";
export const USER_POST_ENDPOINT = API_URL + "/users/posts";
export const CREATE_POST_ENDPOINT = API_URL + "/posts";
export const DELETE_POST_ENDPOINT = API_URL + "/posts";
export const UPDATE_POST_ENDPOINT = API_URL + "/posts";