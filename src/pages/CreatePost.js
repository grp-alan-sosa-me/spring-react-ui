import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Container, Row, Col, Card, Alert} from 'react-bootstrap';
import validator from 'validator';
import { isObjectEmpty } from '../helpers/helpers';
import CreatePostForm from '../components/forms/CreatePostForm';
import { exposures } from '../helpers/exposures';
import { CREATE_POST_ENDPOINT} from'../helpers/endpoints';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import { getUserPosts } from '../actions/postActions';

export default function CreatePost() {
	
	//Adding the errors object to the state. useState({}) is to declare it as Object.
	const [errors, setErrors] = useState({});

	//call the hook useHistory();
	const history = useHistory();

    //call the hook useDispatch();
    const dispatch = useDispatch();
	
	const createPost = async ({ title, content, expirationTime, exposureId }) => {
		const errors = {};
		//using the setErrors function we're adding the errors object to the state.
		setErrors(errors);
		
		//validate fields and send errors if exists
		if(validator.isEmpty(title)){
			errors.title = "Title can't be empty";
		}

        if(validator.isEmpty(content)){
			errors.content = "Content can't be empty";
        }
		
		//Validate if the errors object is not empty
		if(isObjectEmpty(errors)){
			setErrors(errors);
			return;
		}
        
        expirationTime = parseInt(exposureId) === exposures.PRIVATE ? 0 : expirationTime;

        //async function version this one uses promises.
        try{
            const response = await axios.post(CREATE_POST_ENDPOINT, {title, content, expirationTime, exposureId});
            await dispatch(getUserPosts());
            toast.info("Post has been created",{ 
                position: "bottom-center",
                autoClose: 2000
            });
            history.push(`post/${response.data.postId}`);
        }catch(error){
            setErrors({
                createPost: error.response.data.message
            });
        }
	}
	
    return (
		<Container className="mt-5 mb-5">
			<Row>
				<Col sm="12" md= {{span: 10, offset: 1}} lg={{ span : 10, offset: 1 }}>
					<Card body>
						{ errors.auth && (<Alert variant="danger">{ errors.auth }</Alert>)}
						<h3>Create Post</h3><hr></hr>
						<CreatePostForm errors={errors} onSubmitCallback={createPost}></CreatePostForm>
					</Card>
				</Col>
			</Row>
		</Container>
    )
}
