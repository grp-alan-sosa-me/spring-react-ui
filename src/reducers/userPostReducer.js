import { SET_USER_POSTS } from "../actions/types";

const intialState = {
    posts : [],
    fetched: false
}

//Here we're setting the initialState to the state that comes
export default function userPostReducer(state = intialState, action){

    //Here again taking the type and payload from the action object.
    const { type, payload } = action;
    
    switch(type){
        //When the state is this one, return the posts and a boolean
        case SET_USER_POSTS:
            return{
                ...state,
                fetched: payload.fetched,
                posts: payload.posts
            }
        default:
            return state;
    }
}