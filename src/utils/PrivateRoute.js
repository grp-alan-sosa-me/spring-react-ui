//this file check if there's authentication done for a protected resource. If enot
//It will redirect to /signin page.

import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import { useSelector} from 'react-redux'


//In the arguments, we take the component prop sent from the Appjs and make it a generic <Component>
//From App.js we send an object that can have as much parameters needed ...rest allow us to handle this
export default function PrivateRoute({ component: Component, ...rest }){
	
	//Get the loggedIn param from the authReducer.
	const loggedIn = useSelector(state => state.auth.loggedIn);
	
	//decide what to render depending if user is loggedIn
	return(
		<Route
		{...rest} //Besides de <Component> received, send all the props to the route 
		render = { 
			props => loggedIn === true ?
			(<Component {...props}> </Component>): //Using spread operator send all the props to the <Component>
			(<Redirect to="/signin"></Redirect>)
		}
		></Route>
	)
}