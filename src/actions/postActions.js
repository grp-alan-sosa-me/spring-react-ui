import axios from 'axios';
import {SET_USER_POSTS} from './types';
import { USER_POST_ENDPOINT} from '../helpers/endpoints';

/*
Here we use dispatch because once we get the objects from the API dispatch allow us to store the variables
in the global store
*/
export const getUserPosts = () => dispatch => {
    return new Promise( (resolve, reject ) => {
        axios.get(USER_POST_ENDPOINT)
        .then( response => {
            dispatch({
                type:SET_USER_POSTS,
                payload: { fetched: true, posts : response.data }
            });

            resolve(response);
        })
        .catch(error => {
            reject(error);
        })
    });
}