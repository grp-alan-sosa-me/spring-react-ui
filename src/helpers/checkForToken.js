import setAuthToken from "./setAuthToken";
import jwt_decode from 'jwt-decode';
import { setCurrentUser, logoutUser } from '../actions/authActions';
import store from "../store";

/*
This function is called when the application is loaded.
And checks if the token exists in localStorage and sets it the application global state.
*/
const checkForToken = () => {
    if(localStorage.jwtToken){
        //Validate if the token exists to ADD/REMOVE from axios parameters.
        setAuthToken(localStorage.jwtToken);
        
        //decode the token
        const decoded = jwt_decode(localStorage.jwtToken);

        //TODO: call /user to get user details and save it to the store.

        //set the state in the store.
        store.dispatch(setCurrentUser({
            user: decoded,
            loggedIn : true
        }))

        //Get the time in miliseconds
        const currentTime = Math.floor(Date.now() / 1000 );

        //Check if token has expired.
        if(decoded.exp < currentTime){
            //Once the token is found we also need to validate if it hasn't expired. In this case we logout the user.
            store.dispatch(logoutUser()); //Logout action
            window.location.href = "/signin"; //Redirect the user to /signin
        }
    }
}

export default checkForToken;