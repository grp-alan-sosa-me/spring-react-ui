//Here we configure the store and redux
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
/*
Here's we're importing rootReducers from /reducers, since there index.js exists.
we don't need to specify the file name. By just calling the folder it goes directly to the
index because javascript by default looks for a index filename
*/
import rootReducer from './reducers';

//Create the middleware that we apply to our redux
const middleware = [thunk];

//Configure the store with our reducers and devtools so we can see states from chrome
const store = createStore(
    rootReducer,
    composeWithDevTools( applyMiddleware(...middleware) )
);
//composeWithDevTools(applyMiddleware(...middleware))
export default store;