import React, { useState, useEffect } from 'react';
import { useHistory, useParams} from 'react-router-dom';
import { Container, Row, Col, Card, Alert} from 'react-bootstrap';
import validator from 'validator';
import { isObjectEmpty } from '../helpers/helpers';
import CreatePostForm from '../components/forms/CreatePostForm';
import { exposures } from '../helpers/exposures';
import { UPDATE_POST_ENDPOINT, POST_DETAILS_ENDPOINT} from'../helpers/endpoints';
import axios from 'axios';
import { toast } from 'react-toastify';
import { useDispatch } from 'react-redux';
import { getUserPosts } from '../actions/postActions';

export default function EditPost() {
	
    const { id } = useParams();

	//Adding the errors and post object to the state. useState({}) is to declare it as a datatype.
	const [errors, setErrors] = useState({});
    const [post, setPost] = useState(null);

	//call the hook useHistory();
	const history = useHistory();

    //call the hook useDispatch();
    const dispatch = useDispatch();

    useEffect( () => {
        axios.get(`${POST_DETAILS_ENDPOINT}/${id}`).then( response => {
            setPost(response.data);
        }).catch(error => {
            //If error ocurred is because the post doesn't belong to the user, it's private or has expired
            history.push('/');
        });
    }, [id, history]);
	
	const editPost = async ({ title, content, expirationTime, exposureId }) => {
		const errors = {};
		//using the setErrors function we're adding the errors object to the state.
		setErrors(errors);
		
		//validate fields and send errors if exists
		if(validator.isEmpty(title)){
			errors.title = "Title can't be empty";
		}

        if(validator.isEmpty(content)){
			errors.content = "Content can't be empty";
        }
		
		//Validate if the errors object is not empty
		if(isObjectEmpty(errors)){
			setErrors(errors);
			return;
		}
        
        expirationTime = parseInt(exposureId) === exposures.PRIVATE ? 0 : expirationTime;

        //async function version this one uses promises.
        try{
            const response = await axios.put(`${UPDATE_POST_ENDPOINT}/${post.postId}`, {title, content, expirationTime, exposureId});
            await dispatch(getUserPosts());
            toast.info("Post has been updated",{ 
                position: "bottom-center",
                autoClose: 2000
            });
            history.push(`/post/${response.data.postId}`);
        }catch(error){
            setErrors({
                editPost: error.response.data.message
            });
        }
	}
	
    return (
		<Container className="mt-5 mb-5">
			<Row>
				<Col sm="12" md= {{span: 10, offset: 1}} lg={{ span : 10, offset: 1 }}>
					<Card body>
						{ errors.auth && (<Alert variant="danger">{ errors.auth }</Alert>)}
						<h3>Edit Post</h3><hr></hr>
						{ post && (<CreatePostForm 
                        errors={errors} 
                        onSubmitCallback={editPost}
                        postTitle = {post.title}
                        postContent = {post.content}
                        postExposureId = {post.exposure.id}
                        textButton = "Edit post"
                        ></CreatePostForm>)}
					</Card>
				</Col>
			</Row>
		</Container>
    )
}
