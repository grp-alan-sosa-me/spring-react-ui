//Import useState so we can use the state redux
import React, { useState } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { exposures } from '../../helpers/exposures';

export default function CreatePostForm({errors, onSubmitCallback, 
	postTitle = "", postContent = "", postExposureId = exposures.PUBLIC, postExpirationTime = 60,
	textButton = "Create post"}){

	const [title, setTitle] = useState(postTitle);
    const [content, setContent] = useState(postContent);
    const [expirationTime, setExpirationTime] = useState(postExpirationTime);
    const [exposureId, setExposureId] = useState(postExposureId);
	
	const submitForm = (e) => {
		e.preventDefault();
		//Trigger the function from whoever
		onSubmitCallback({title, content, expirationTime, exposureId})
	}
	
	return(
	<Form onSubmit={submitForm}>
		<Form.Group control="title">
			<Form.Label>Title</Form.Label>
			<Form.Control
				type ="text"
				value = {title}
				onChange = { e => setTitle(e.target.value) }
				placeholder = "e.g for loop snippet"
				isInvalid = {errors.title}
			/>
			<Form.Control.Feedback type = "invalid">
				{ errors.title }
			</Form.Control.Feedback>
		</Form.Group>
        <Row>
            <Col md="6" xs="12">
                <Form.Group controlId="expirationTime">
                    <Form.Label>Expiration Time</Form.Label>
                    <Form.Control 
                        disabled={ parseInt(exposureId) === exposures.PRIVATE }
                        as = "select" value={expirationTime}
                        onChange={ (event) => { setExpirationTime(event.target.value)}}
                        >
                        <option value="30"> 30 minutes </option>
                        <option value="60"> 1 hour </option>
                        <option value="120"> 2 hours </option>
                        <option value="360"> 6 hourse </option>
                        <option value="720"> 12 hours </option>
                        <option value="1440"> a day </option>
                    </Form.Control>
                </Form.Group>
                <Form.Control.Feedback type="invalid">
                    { errors.expirationTime }
                </Form.Control.Feedback>
            </Col>
            <Col md="6" xs="12">
                <Form.Group controlId="exposureId">
                    <Form.Label>Post Type</Form.Label>
                    <div>
                        <Form.Check
                            onChange = { (event) => setExposureId(event.target.value)}
                            checked = { parseInt(exposureId) === exposures.PRIVATE }
                            value = {exposures.PRIVATE}
                            inline label = "Private"
                            name = "exposureId"
                            type = "radio"
                        ></Form.Check>

                        <Form.Check
                            onChange = { (event) => setExposureId(event.target.value)}
                            checked = { parseInt(exposureId) === exposures.PUBLIC }
                            value = {exposures.PUBLIC}
                            inline label = "Public"
                            name = "exposureId"
                            type = "radio"
                        ></Form.Check>

                    </div>
                </Form.Group>
                <Form.Control.Feedback type="invalid">
                    { errors.expirationTime }
                </Form.Control.Feedback>
            </Col>
        </Row>

        <Form.Group control="content">
			<Form.Label>Content</Form.Label>
			<Form.Control
                as="textarea"
                rows={10}
				value = {content}
				onChange = { e => setContent(e.target.value) }
				placeholder = "e.g for loop snippet"
				isInvalid = {errors.content}
			/>
			<Form.Control.Feedback type = "invalid">
				{ errors.content }
			</Form.Control.Feedback>
		</Form.Group>
		<Button variant="primary" type="submit">{textButton}</Button>
	</Form>
	);
}