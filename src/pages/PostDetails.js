import React, { useState, useEffect }from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { POST_DETAILS_ENDPOINT } from '../helpers/endpoints'
import { Card, Jumbotron, Button } from 'react-bootstrap'
import axios from 'axios'
import moment from 'moment'
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { toast } from 'react-toastify'
import { downloadTextAsFile } from '../helpers/helpers'

export default function PostDetails() {

    /*
    useParams is another hook that let us get the variable passed through the url params from Post.js

    Take note we're using ES6 to take the param id. from the array. This is because in App.js we declared
    the parameter passed as 'id' when the <Route> was declared using "/post/:id"
    */
    const { id } = useParams()
    const [post, setPost] = useState(null);
    const [fetching, setFetching] = useState(false);
    const history = useHistory();

    useEffect( () => {
        axios.get(`${POST_DETAILS_ENDPOINT}/${id}`).then( response => {
            setPost(response.data);
            setFetching(false);
        }).catch(error => {
            //If error ocurred is because the post doesn't belong to the user, it's private or has expired
            history.push('/');
        });
    }, [id, history]);


    return (
        <div className = "pb-4">
            { (!fetching && post) && (<React.Fragment>
                <Jumbotron>
                    <h1>{post.title}</h1>
                    <p>Created by : {post.user.firstName}, { moment (post.createdAt).fromNow() }</p>
                </Jumbotron>

                <Card>
                    <Card.Header>
                        <Button 
                        variant="primary"
                        className="mr-2"
                        size="sm"
                        onClick= { () => {
                            downloadTextAsFile(post.postId, post.content)
                        }}>Download</Button>

                        <CopyToClipboard
                            text={ post.content }
                            onCopy={() => {
                                toast.info("Copied to clipboard",{ 
                                    position: "bottom-center",
                                    autoClose: 2000
                                });
                            }}>

                            <Button
                            variant="primary"
                            size="sm"
                            >Copy to clipboard</Button>

                        </CopyToClipboard>      
                    </Card.Header>
                    <Card.Body>
                        <SyntaxHighlighter showLineNumbers = "true" language="javascript">
                            {post.content}
                        </SyntaxHighlighter>
                    </Card.Body>
                </Card>
            </React.Fragment>)}
        </div>
    )
}
