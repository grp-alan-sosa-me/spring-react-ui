import React, {useState, useEffect} from 'react';
import { Jumbotron } from 'react-bootstrap';
import Post from '../components/posts/Post';
import NoPost from '../components/utils/NoPost';
import Placeholder from '../components/utils/Placeholder';
import { useSelector, useDispatch } from 'react-redux';
import {getUserPosts} from '../actions/postActions';
import { toast } from 'react-toastify';

export default function UserPosts() {

    const [fetching, setFetching] = useState(false);
	const posts = useSelector( state => state.posts.posts);
	const fetched = useSelector( state => state.posts.fetched);
	const dispatch = useDispatch();

    /*
    Ths is called when the component is loaded AND when there's a change in the state.
    Since we're updating the state in the action call, the useEffect will run in an
    Infinite loop. To avoid this behaviour we send a 2nd parameter '[]' which is an array
    that will contain the variables that are in the state and so redux can 'listen' to changes
    in the state. We don't want to listen for any change so just send a [] empty array.
    */
    useEffect( () => {
		//Declare an async function. useEffect doesn't support async functions in the callback.
		//We have to wrap an async function and then send it. to the useEffect().
        async function fetchedPosts(){

			if(!fetched){
				try{
					//First declare the setfetching true.
					setFetching(true);
					//Since we're in an async function we can wait for the dispatch to complete the action
					await dispatch( getUserPosts() );
					//After the action is completed. set the fetching state to false.
					setFetching(false);
				}catch(error){
					toast.error( error.response.data.message, { position: toast.POSITION.BOTTOM_CENTER, autoClose:2000});
				}
			}
		}
		fetchedPosts();
    }, [dispatch, fetched]); 

    return (
        <div>
            <Jumbotron>
                <h1>My Posts</h1>
            </Jumbotron>
			{ fetching && <Placeholder></Placeholder>}
			{ !fetching && posts.length === 0 && <NoPost text="You haven't posted anything yet"></NoPost>}
            <div>
                { posts.map( post => <Post key={post.postId} post={post} renderControls={true}></Post>)}
            </div>
        </div>
    )
}
