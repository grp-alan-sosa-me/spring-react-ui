import React from 'react'
import { Nav, Navbar, NavDropdown } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch} from 'react-redux'
import { logoutUser } from '../actions/authActions'

export default function Navigation() {
    const loggedIn = useSelector( state => state.auth.loggedIn);
    const user = useSelector( state => state.auth.user);

    //use the dispatch to call actions.
    const dispatch = useDispatch();
    /*
    user {
        exp: 12312213,
        sub: "emai@domain.com"
    }
    */
    
    return (
        <div>
            <Navbar bg="dark" variant="dark" expand="lg">
                <Navbar.Brand as={NavLink} to="/">React Spring</Navbar.Brand>
                <Navbar.Toggle aria-controls="main-menu"></Navbar.Toggle>
                <Navbar.Collapse id="main-menu">
                    <Nav className="mr-auto">
                        {   /*
                            This is ternary condition but only with if. 
                            if(true){
                                render
                            }

                            we're not having an else statement.
                            */
                            loggedIn && <Nav.Link as={NavLink} to="/createpost">Create Post</Nav.Link> 
                        }
                        
                    </Nav>
                    <Nav>
                        { !loggedIn ?  //ternary condition. <React.Fragment> is needed because we need to return a <Component>
                        (
                            <React.Fragment>
                                <Nav.Link as={NavLink} to="/signup">Create Account</Nav.Link>
                                <Nav.Link as={NavLink} to="/signin">Sign In</Nav.Link>
                            </React.Fragment>
                        ):
                        <React.Fragment>
                            <NavDropdown title={ user.sub } id="menu-dropdown">
                                <NavDropdown.Item as={NavLink} to='/posts'>My posts</NavDropdown.Item>
                                <NavDropdown.Item onClick= { () => dispatch(logoutUser())}>Close Session</NavDropdown.Item>
                            </NavDropdown>
                        </React.Fragment>
                        }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}
