import axios from 'axios';

/*
This methods add or delete the token from axios parameters.
Here we're declaring an arror function that has only one sentence. so we don't need to have (token) => {}
*/
const setAuthToken = token => {
    if(token){
        axios.defaults.headers.common["Authorization"] = token;
    } else{
        //Here we delete a property from a Javascript object.
        delete axios.defaults.headers.common["Authorization"];
    }
}

export default setAuthToken;