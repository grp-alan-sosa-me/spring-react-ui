import axios from 'axios';
import React, {useState, useEffect} from 'react';
import { Jumbotron } from 'react-bootstrap';
import { PUBLIC_POSTS_ENDPOINT } from '../helpers/endpoints';
import Post from '../components/posts/Post';
import NoPost from '../components/utils/NoPost';
import Placeholder from '../components/utils/Placeholder';

export default function Posts() {

    const [posts, setPosts] = useState([]);
    const [fetching, setFetching] = useState(true);

    /*
    Ths is called when the component is loaded AND when there's a change in the state.
    Since we're updating the state in the axios call, the useEffect will run in an
    Infinite loop. To avoit this behaviour we send a 2nd parameter '[]' which is an array
    that will contain the variables that are in the state and so redux can 'listen' to changes
    in the state. We don't want to listen for any change so just send a [] empty array.
    */
    useEffect( () => {
        axios.get(PUBLIC_POSTS_ENDPOINT).then( response => {
            setPosts(response.data);
            setFetching(false);
        }).catch(error => {
            setFetching(false);
        });
    }, []); 

    return (
        <div>
            <Jumbotron>
                <h1>Last Public Posts</h1>
            </Jumbotron>
            { fetching && <Placeholder></Placeholder>}
            { !fetching && posts.length === 0 && <NoPost text="No public posts available"></NoPost>}
            <div>
                { posts.map( post => <Post key={post.postId} post={post} renderControls = {false}></Post>)}
            </div>
        </div>
    )
}
