import axios from 'axios';
import { LOGIN_ENDPOINT, CREATE_USER_ENDPOINT } from '../helpers/endpoints';
import { SET_CURRENT_USER } from "../actions/types";
import jwt_decode from 'jwt-decode';
import setAuthToken from '../helpers/setAuthToken';

//in case we want to call another action we return dispatch becase we want to save the token in store.
export const loginUser = (userData) => dispatch => {
	return new Promise((resolve, reject) => {
		//use axios to make a Post request to the backend.
		axios.post(LOGIN_ENDPOINT, userData, {
			headers: { 'Accept' : 'application/json', 'Content-type': 'application/json'}
		}).then(response => { //If the promise can be resolved
			//Get authorization and userId headers form the response.
			const { authorization } = response.headers;
			
			//save the storage in local memory
			localStorage.setItem('jwtToken', authorization);

			//Set the token to axios
			setAuthToken(authorization);

			//jwt-decode library
			const decoded = jwt_decode(authorization);

			//Here we want to change the names of the variables, so no ES6 :C
			//set the action object so the authReducer is able to set the state.
			dispatch(setCurrentUser( {user: decoded, loggedIn : true} ));

			resolve(response) //send the response to the resolve function
		}).catch(error => { //if an error ocurred.
			reject(error) //send the error to the reject function.
		}) 
	})
}

export const createUser = (userData) => dispatch => {
	return new Promise( (resolve, reject ) =>{
		axios.post(CREATE_USER_ENDPOINT, userData, {
			headers: { 'Accept' : 'application/json', 'Content-type': 'application/json'}
		}).then( response => {
			resolve(response)
		})
		.catch( error => {
			reject(error)
		})
	})
}


//logoutUser can accept a call with empty parameters thanks to the () 
export const logoutUser = () => dispatch => {
	//Remove the token from localStorage.
	localStorage.removeItem('jwtToken');
	//Remove it from axios params
	setAuthToken(false);
	//set the state.
	dispatch(setCurrentUser({
		user: {},
		loggedIn : false
	}));


}

/*
Sets the current user so the reducer can set it in the application's global state.
In the params we get the user and loggedIn params from the object sent in the loginUser action.
*/
export const setCurrentUser = ({user, loggedIn}) => {
	return {
		type: SET_CURRENT_USER,
		payload : { user, loggedIn }
	};
}