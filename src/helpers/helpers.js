export const isObjectEmpty = (obj) => {
	//If object doesn't have keys and if the constructor is 'Object' means is empty
	return Object.keys(obj).lenght === 0 && obj.constructor === Object;
}

export const downloadTextAsFile = (filename, text) => {
	let element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);
	
	//Make the element not visible in html
	element.style.display = 'none'
	//Add the element to the DOM
	document.body.appendChild(element);
	//Simulate a Click event
	element.click();
	//remove after the click is done.
	document.body.removeChild(element);
}