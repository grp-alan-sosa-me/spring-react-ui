import { SET_CURRENT_USER } from "../actions/types";

const intialState = {
    loggedIn : false, 
    user : {}
}

//Here we're setting the initialState to the state that comes
export default function authReducer(state = intialState, action){
    //This is ECMA I believe.
    //We're only "extracting" type and payload from the action object.
    //In java the equivalent is action.type, action.payload
    const { type, payload } = action;
    
    switch(type){
        case SET_CURRENT_USER:
            /*
            ...state is using '...' which are SPREAD OPERATORS.
            which means that return is returning a new object that contains the parameters of 
            state AND is modifying loggedIn and user which are also within state.

            So we're only modifying those 2 parameters and the rest of state stays the same.
            */
            return{
                ...state,
                loggedIn: payload.loggedIn,
                user: payload.user
            }
        default:
            return state;
    }
}