//Import useState so we can use the state redux
import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

//in the parameters we're using the Spread? operator
//this is a stateles componentes but, thanks to useState we're using hooks so the component
//can have a state
export default function SignInForm({ errors, onSubmitCallback } ){
	
	/*
	* Here we're using REACT HOOKs. We just need to define the name of the property to change
	* and it's setter.
	* below we're adding email and password to the state and with useState("") we add them 
	* as empty strings.
	*/
	const [ email, setEmail ] = useState("");
	const [ password, setPassword ] = useState("");
	
	//Arrow function. 'e' is the event parameter when clicked the submit button.
	const submitForm = (e) => {
		//Prevent the default behaviour.
		e.preventDefault();
		//send the 2 componentes to the login() function sent from SignIn.js
		onSubmitCallback({email, password})
		
	}
	
	return(
	<Form onSubmit={submitForm}>
		<Form.Group control="email">
			<Form.Label>Email</Form.Label>
			<Form.Control
				type ="email"
				value = {email}
				/*
				In onChange we're declaring an arrow function that uses the setEmail method 
				to set the value to the email param
				*/	
				onChange = { e => setEmail(e.target.value) }
				placeholder = "my.email@domain.com"
				//when the errors parameter exists sent from the SignIn component then trigger the error
				//behaviour
				isInvalid = {errors.email}
			/>
			<Form.Control.Feedback type = "invalid">
				{ errors.email }
			</Form.Control.Feedback>
		</Form.Group>
		<Form.Group control="password">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type ="password"
				value = {password}
				/*
				In onChange we're declaring an arrow function that uses the setEmail method 
				to set the value to the email param
				*/	
				onChange = { e => setPassword(e.target.value) }
				placeholder = "password"
				//when the errors parameter exists sent from the SignIn component then trigger the error
				//behaviour
				isInvalid = {errors.password}
			/>
			<Form.Control.Feedback type = "invalid">
				{ errors.password }
			</Form.Control.Feedback>
		</Form.Group>
		<Button variant="primary" type="submit">Sign in</Button>
	</Form>
	);
}