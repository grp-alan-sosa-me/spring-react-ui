//this function allow us to combine multiple reducers into a single one.
import { combineReducers } from "redux";
import authReducer from './authReducer';
import userPostReducer from './userPostReducer'

export default combineReducers({
    auth : authReducer,
    posts: userPostReducer
})