import React from 'react';
import nodata from '../../assets/nodata.svg';

export default function NoPost({text}) {
    return (
        <div className = "no-post-component">
            <div className="post-image-container">
                <object type = "image/svg+xml" data={nodata}>
                    Geeeezz.. Use a newer browser you boomer
                </object>
                <p>{text}</p>
            </div>
        </div>
    )
}
