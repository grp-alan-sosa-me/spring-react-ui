import React, { useState, useEffect } from 'react'
import SignInForm from '../components/forms/SignInForm'
import { Link, useHistory } from 'react-router-dom'
import { Container, Row, Col, Card, Alert} from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import validator from 'validator'
import { isObjectEmpty } from '../helpers/helpers'
import { loginUser } from '../actions/authActions'

export default function SignIn() {
	
	//Adding the errors object to the state. useState({}) is to declare it as Object.
	const [errors, setErrors] = useState({});
	
	//This will allow us to call actions. The action that will send data to the backend
	const dispatch = useDispatch();

	/*
	useSelector allow the component to get data from the store. 
	Take note that in useSelector we're using an arrow function that returns from state object
	the auth reducer and the loggedIn parameter

	if you check the reducers/index.js, the authReducer is declared 'auth'

	so we're just taking the loggedIn parameter from the state.
	*/
	const loggedIn = useSelector( state => state.auth.loggedIn);

	//call the hook useHistory();
	const history = useHistory();

	//This is the equivalent of componentDidMount() when not using hooks
	useEffect( () => {
		//This function is called when the component is started
		if(loggedIn){
			//Redirect the user to main page.
			history.push("/");
		}
	});
	
	//login function that we're sending to <SignInForm onSubmitCallback function>
	const login = ({ email, password }) => {
		const errors = {};
		//using the setErrors function we're adding the errors object to the state.
		setErrors(errors);
		
		//validate fields and send errors if exists
		if(!validator.isEmail(email)){
			errors.email = "The email is invalid";
		}
		
		if(validator.isEmpty(password)){
			errors.password = "Password field can't be empty"
		}
		
		//Validate if the errors object is not empty
		if(isObjectEmpty(errors)){
			setErrors(errors);
			return;
		}
		/*
		Call the login action in authActions using dispatch.
		Thanks to ES6 we don't need to declare the object like this:
		{ email : email, password : password }
		*/
		dispatch( loginUser( { email, password } ) )
			.then(response => {
				//resolve
			})
			.catch(error => {
				//set the errors in the error state var.
				setErrors({ auth : "Failed to login" });
			});
	}
	
    return (
		<Container className="mt-5">
			<Row>
				<Col sm="12" md= {{span: 8, offset: 2}} lg={{ span : 6, offset: 3 }}>
					<Card body>
						{ errors.auth && (<Alert variant="danger">{ errors.auth }</Alert>)}
						<h3>Sign in</h3><hr></hr>
						<SignInForm errors={errors} onSubmitCallback={login}></SignInForm>
						<div className = "mt-4">
							<Link to={"/signup"}>Register here</Link>
						</div>
					</Card>
				</Col>
			</Row>
		</Container>
    )
}
