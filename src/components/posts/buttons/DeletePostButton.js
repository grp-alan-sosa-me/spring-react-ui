import React from 'react';
import axios from 'axios';
import { Button } from 'react-bootstrap';
import { confirmAlert } from 'react-confirm-alert';
import { DELETE_POST_ENDPOINT } from '../../../helpers/endpoints';
import { useDispatch } from 'react-redux';
import { getUserPosts } from '../../../actions/postActions';
import { toast } from 'react-toastify';


export default function DeletePostButton({ postId, title }) {

    const dispatch = useDispatch();

    const createAlert = () => {
        confirmAlert({
            title : "Delete post",
            message : `Are you sure you want to delete the post ${title} ?`,
            buttons: [
                {
                    label: "Yes",
                    onClick : () => { deletePost() }
                },
                {
                    label: "No",
                    onClick : () => { return false;}
                }
            ]
        });
    }

    const deletePost = async () =>{
        try{
            await axios.delete(`${DELETE_POST_ENDPOINT}/${postId}`);
            await dispatch(getUserPosts())
            toast.info("Post has been deleted",{ 
               position: "bottom-center",
               autoClose: 2000
            });
        }catch(error){
            toast.info(error.response.data.message,{ 
                position: "bottom-center",
                autoClose: 2000
            });
        }
    }
    //<Button variant="primary" size="sm">Delete</Button>
    return (
        <Button 
        onClick = {createAlert}
        variant="primary" 
        size="sm">Delete</Button>
    )
}