import React, { useState, useEffect } from 'react'
import { Link, useHistory } from 'react-router-dom'
import { Container, Row, Col, Card, Alert} from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import validator from 'validator'
import { isObjectEmpty } from '../helpers/helpers'
import { createUser, loginUser } from '../actions/authActions'
import SignUpForm from '../components/forms/SignUpForm';

export default function SignUp() {
	
	//Adding the errors object to the state. useState({}) is to declare it as Object.
	const [errors, setErrors] = useState({});
	
	//This will allow us to call actions. The action that will send data to the backend
	const dispatch = useDispatch();

	/*
	useSelector allow the component to get data from the store. 
	Take note that in useSelector we're using an arrow function that returns from state object
	the auth reducer and the loggedIn parameter

	if you check the reducers/index.js, the authReducer is declared 'auth'

	so we're just taking the loggedIn parameter from the state.
	*/
	const loggedIn = useSelector( state => state.auth.loggedIn);

	//call the hook useHistory();
	const history = useHistory();

	//This is the equivalent of componentDidMount() when not using hooks
	useEffect( () => {
		//This function is called when the component is started
		if(loggedIn){
			//Redirect the user to main page.
			history.push("/");
		}
	});
	
	//login function that we're sending to <SignInForm onSubmitCallback function>
	const signup = ({ firstName, lastName, email, password }) => {
		const errors = {};
		//using the setErrors function we're adding the errors object to the state.
		setErrors(errors);
		
        //validate fields and send errors if exists
		if(validator.isEmpty(firstName)){
			errors.firstName = "First Name is invalid";
		}
        
        if(validator.isEmpty(lastName)){
			errors.lastName = "Last Name is invalid";
		}

		//validate fields and send errors if exists
		if(!validator.isEmail(email)){
			errors.email = "The email is invalid";
		}
		
		if(!validator.isLength( password, { min : 8 , max: 30 } ) ) {
			errors.password = "Password field must be between 8 and 30 characters"
		}
		//Validate if the errors object is not empty
		if(isObjectEmpty(errors)){
			setErrors(errors);
			return;
		}
		/*
		Call the login action in authActions using dispatch.
		Thanks to ES6 we don't need to declare the object like this:
		{ email : email, password : password }
		*/
		dispatch( createUser( { firstName, lastName, email, password } ) )
			.then(response => {
                dispatch(loginUser({email, password}));
				//resolve
                //Log the user
			})
			.catch(error => {
				//set the errors in the error state var.
				setErrors({ registerError : error.response.data.message });
			});
	}
	
    return (
		<Container className="mt-5">
			<Row>
				<Col sm="12" md= {{span: 8, offset: 2}} lg={{ span : 6, offset: 3 }}>
					<Card body>
                        { errors.registerError && (<Alert variant="danger">{ errors.registerError }</Alert>)}
						<h3>Create Account</h3><hr></hr>
						<SignUpForm errors={errors} onSubmitCallback={signup}></SignUpForm>
						<div className = "mt-4">
							<Link to={"/signin"}>Already have an account? Sign in here</Link>
						</div>
					</Card>
				</Col>
			</Row>
		</Container>
    )
}
